import abc
'''这个Encrpyter是一个抽象类，抽象类比如说，水果，动物，交通工具等名词，它只是名词，并不个体存在
而抽象类就有抽象函数（注意，编程的函数跟数学的函数不是一个东西，编程函数是一段为了实现某个功能而用def等语句进行区分的代码，从而达到避免逻辑混论和代码安全
重复利用（功能，就是function，中文翻译直接叫函数））抽象函数就比如： 父类动物会移动，就是move（）函数，那继承于其分类的子类 马 和 鸟这两中动物也会移动，
问题是马的移动方式是地上跑而鸟的移动方式是天上飞，这就需要抽象函数来实现出两种相同的函数执行不同的功能，（或者可以通过不同的函数名称实现，对新手来说挺
好的，但很乱容易出bug和混淆，所以我更喜欢前者），这是一个典型的多态，构建多态有时要结合现实生活逻辑。另外，抽象函数有一个功能是用于强制时子类有同样的功能，既然父类 动物有移动函数那么
其继承的子类肯定也得有这个移动函数。

（为什么说了这么多，好像要把你教会似的，不知道你看没看懂，不管了，写都写出来了，不懂可以问我或者找百度，啊不百度并不靠谱，得找Google :D
或者你不看也行）'''

class Encrypter(abc.ABC):
    def __init__(self, plank_text, secret):
        self.__plank_text = plank_text
        self.__secret = secret
        self.alphabet = ['A','B','C','D','E','F','G','H','I','J','K','L','M',
            'N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
        self.result = []

    def getPlank_text(self):
        return self.__plank_text

    def getSecret(self):
        return self.__secret

    @abc.abstractmethod
    def encryption(self):
        pass

    def __str__(self):
        return str(self.result)

    def __repr__(self):
        self.__str__()

'''凯撒加密法'''
class Caesar_cipher(Encrypter):
    def __init__(self, plank_text, secret: int, direction):
        super().__init__(plank_text, secret)
        self.__direction = direction
        self.shift_apha = []
        self.found_lis = []

        if self.__direction == 'L':
            self.shift_left(secret)
        else:
            self.shift_right(secret)

        self.locate_index(plank_text)
        self.encryption()    

    #字母表根据偏移量向左偏移
    def shift_left(self,shift_amount):
        index1 = 0
        index2 = 0
        while index1 < len(self.alphabet):
            if index1 + shift_amount < len(self.alphabet):
                self.shift_apha.append(self.alphabet[index1+shift_amount])
                
            else:
                self.shift_apha.append(self.alphabet[index2])
                index2 += 1
            index1+=1
            
    #字母表根据偏移量向右偏移
    def shift_right(self,shift_amount):
        index1 = len(self.alphabet)-shift_amount
        index2 = 0
        while index1 < len(self.alphabet):
            if index1 - shift_amount >= 0: 
                self.shift_apha.append(self.alphabet[index1])
                index1+=1
        while index2 < len(self.alphabet)-shift_amount:
            self.shift_apha.append(self.alphabet[index2])
            index2+=1
    #在得出偏移字母上对明码字母在字母表中的位置进行索引定位
    def locate_index(self,text = ''):
        for i in text:
            for k in self.alphabet:
                if i == k:
                    index = self.alphabet.index(k)
                    self.found_lis.append(index)
    #根据得出的定位在偏移字母上进行加密
    def encryption(self):
        encrypted_str = ''
        for i in self.found_lis:
            encrypted_str = encrypted_str + self.shift_apha[i]
        self.result = encrypted_str

'''阿特巴希加密法'''
class Atabash_cipher(Encrypter):
    def __init__(self, plank_text = '', secret = ''):
        super().__init__(plank_text, secret)
        self.__plank_text = plank_text
        self.found_lis = []
        self.reverse_alpha = self.reverse()

        self.locate_index(plank_text)
        self.encryption()

    #手动倒置字母表排序
    def reverse(self):
        index = len(self.alphabet)-1
        reverse_alphabet = []
        while index > -1:
            reverse_alphabet.append(self.alphabet[index])
            index-=1
        return reverse_alphabet

        
    #利用明码字母在字母表中的位置对颠倒字母表中的位置进行定位
    def locate_index(self, text = ''):

        for i in text:
            for k in self.alphabet:
                if i == k:
                    index = self.alphabet.index(k)
                    self.found_lis.append(index)
    #加密
    def encryption(self):
        encrypted_str = ''
        for i in self.found_lis:
            encrypted_str = encrypted_str + self.reverse_alpha[i]
      
        self.result = encrypted_str

'''栅栏加密法'''
class Fail_fence_cipher(Encrypter):
    def __init__(self, plank_text = '', secret = ''):
        super().__init__(plank_text, secret)
        self.lis1 = []
        self.lis2 = []
        self.lis3 = []
        self.__plank_text = plank_text
        self.frequence = secret - 1
        self.encryption()

    #加密，其实这根本不是栅栏加密，当时做的时候算法偏离了，这个算法更像洗牌的方式，将明码分割成两半
    #然后依次从这两半中获取字母放在一起从而实现加密效果，缺点是加密次数过多会蹦出明码。
    def encryption(self):
        index = 0
        for i in range(self.frequence):
            while index < len(self.__plank_text):
                if index % 2 == 0:
                    self.lis1.append(self.__plank_text[index])
                else:
                    self.lis2.append(self.__plank_text[index])
                index+=1
            self.lis3 = self.lis1 + self.lis2
            index = 0
            self.__plank_text = ''
            while index < len(self.lis3):
                self.__plank_text = self.__plank_text + self.lis3[index]
                index+=1
            print(self.__plank_text)
            self.lis1 = []
            self.lis2 = []
            self.lis3 = []
            index = 0

        self.result = self.__plank_text
    #为什么没有解密？因为不会 （摊手）

'''维吉尼亚加密法'''
class Vigenere_cipher(Encrypter):
    def __init__(self, plank_text, secret, option: str):
        super().__init__(plank_text, secret)
        self.__plank_text = plank_text
        self.__secret = secret
        
        self.secret_expanded_lenght = ''
        self.found_plank_lis = []
        self.found_shift_lis = []
        self.checkSecret()
        if option != 'D':
            self.locate_Plank_position()
        self.locate_shift_position()
        if option == 'E':
            self.encryption()
        else:
            self.decryption()

    #检测密钥的长度
    def checkSecret(self):
        plank_count = 0
        secret_count = 0
        for i in self.__plank_text:
            if i != ' ':
                plank_count+=1

        for k in self.__secret:
            if k != ' ':
                plank_count+=1

        if plank_count != secret_count:
            self.__secret = self.expend_secret()

    #通常来说维吉尼亚加密法的密钥长度比明码长度短，因此需要以重复密钥的字母来延长密钥长度从而达到与明码长度一致
    def expend_secret(self):
        expanded_secret = ''
        count = 0
        for i in self.__plank_text:
            count+=1

        index1 = 0
        index = 0
        while index1 < count:
            if index == len(self.__secret):
                index = 0
            expanded_secret = expanded_secret + self.__secret[index]
            index+=1
            index1+=1
        return expanded_secret

    #在字母表中定位明码字母的索引位置
    def locate_Plank_position(self):
        for i in self.__plank_text:
            for k in self.alphabet:
                if i == k:
                    letter_position = self.alphabet.index(k)
                    self.found_plank_lis.append(letter_position)
    #在字母表中定为密钥的索引位置来获取偏移量
    def locate_shift_position(self):
        for i in self.__secret:
            for k in self.alphabet:
                if i == k:
                    shift = self.alphabet.index(k)
                    self.found_shift_lis.append(shift)

    # 有些复杂，给你讲解一下。维吉尼亚加密法是从凯撒加密法原有的算法中改进而来，利用一张表格把所有的字母偏移按照顺序迭代起来，像这样:
    #0   1    2    3    4    5
    #1, 'A', 'B', 'C', 'D', 'E' ...
    #2, 'B', 'C', 'D', 'E', 'F' ...
    #2, 'C', 'D', 'E', 'F', 'G' ...
    #4, 'D', 'E', 'F', 'G', 'H' ...
    #在这表格中，第一行的横向字母用作明码的参考，而纵向的字母则用作密钥的参考。根据明码和密钥的的对应出的在字母表的位置则是密码，
    #举个栗子：明码字母是C，密钥字母是D，则这俩字母在表格中对应的位置则是 （3，4）也就是F
    #有效的解决了被频率分析攻击的问题

    #加密，这里执行着上面注释所说的一切
    def encryption(self):
        index = 0
        index2 = 0
        encrypted_letter = []
        encrypted = ''
        count=0
    
        for i in self.found_shift_lis:
            while index < len(self.alphabet):
                if index + i < len(self.alphabet):
                    encrypted_letter.append(self.alphabet[index + i])
                
                else:
                    encrypted_letter.append(self.alphabet[index2])
                    index2+=1
                index+=1
            index = 0
            index2 = 0
            
            if count != len(self.found_plank_lis):
                encrypted = encrypted + encrypted_letter[self.found_plank_lis[count]]
                count+=1
            encrypted_letter = []
        
        self.result = encrypted

    #解密
    def decryption(self):
        index = 0
        index2 = 0
        encrypted_letter = []
        dencrypted = ''
        count=0
    
        for i in self.found_shift_lis:
            while index < len(self.alphabet):
                if index + i < len(self.alphabet):
                    encrypted_letter.append(self.alphabet[index + i])
                
                else:
                    encrypted_letter.append(self.alphabet[index2])
                    index2+=1
                index+=1
            index = 0
            index2 = 0
            
            for i in self.__plank_text[count]:
                for k in encrypted_letter:
                    if i == k:
                        plank_location = encrypted_letter.index(k)
                        dencrypted = dencrypted + self.alphabet[plank_location]
                        encrypted_letter = []
                count+=1
        
        self.result = dencrypted

'''转换加密法'''
#可以从这视频中看讲解https://www.youtube.com/watch?v=bcyUJK1BvHw
class Transposition_cipher(Encrypter):
    def __init__(self, plank_text, secret):
        super().__init__(plank_text, secret)
        self.__plank_text = plank_text
        self.__secret = secret
        self.plank_list = []
        self.transform_list = []
        self.ordered_list = []
        self.built_list()
        self.transform()
        self.change_order()
        self.encryption()

    #根据密钥和明码的长度搭建数组
    def built_list(self):
        row = len(self.__secret)
        column = 1
        count = 0

        while count < len(self.__plank_text):
            for k in range(column):
                lis = []
                for i in range(row):
                    if count < len(self.__plank_text):
                        if self.__plank_text[count] == ' ':
                            count += 1
                        lis.append(self.__plank_text[count])
                        count+=1

                    else:
                        lis.append('X')
                while len(lis) < len(self.__secret):
                    lis.append('X')
                self.plank_list.append(lis)

    #使字母数组从横向转换为纵向
    def transform(self):
        rowCount = 0
        columnCount = 0
        innerlis = []
        while len(self.plank_list[0]) >= columnCount and len(self.plank_list[0]) != rowCount:
            if columnCount != len(self.plank_list):
                
                innerlis.append(self.plank_list[columnCount][rowCount])
                columnCount+=1
            else:
                
                self.transform_list.append(innerlis)
                innerlis = []
                columnCount = 0
                rowCount+=1   

    #根据密钥的顺序更改数组的位置
    def change_order(self):
        count = 0
        for i in self.__secret:
            if int(i)-1 > len(self.transform_list)-1:
                i = len(self.transform_list)
            self.ordered_list.append(self.transform_list[int(i)-1])

    #加密，纵向阅读字母数组得出密码
    def encryption(self):
        rowCount = 0
        string = ''
        while len(self.ordered_list) > rowCount:
            columnCount = 0
            while len(self.ordered_list[rowCount]) > columnCount:
                string += self.ordered_list[rowCount][columnCount]
                columnCount += 1
            rowCount += 1
        self.result = string

    #这里也没有解密，还是因为人类的能力是有限的


#这里是运行上面各类加密法的控制代码，很杂，没什么好看的

def main():
    run = True
    while run:
        start = input('欢迎使用Jay写的加密器, 选择 (y/n) 以继续: ').lower()
        if start == 'y':
            print(
                '加密选项:\n'
                '凯撒加密 #1\n'
                '阿特巴希加密 #2\n'
                '栅栏加密 #3\n'
                '维吉尼亚加密 #4\n'
                '转换加密 #5\n'
                ' \n'
                '温馨提示：\n'
                '加密器只适应于英语字母，拼音也可以\n'                
                '请勿用此工具与公安机关进行交流，会坐牢的 :）\n'
            )
            option = input('请选择你的加密方式：')
            if option == '1':
                print('#凯撒加密#')
                plank_text = input('请输入明码: ').upper()
                shift_amount = int(input('请输入偏移量: '))
                shift_direction = input('请输入左右方向 (r/l): ').upper()
                my_text = Caesar_cipher(plank_text, shift_amount, shift_direction)
                print(f'您的加密信息是:{my_text}')

            elif option == '2':
                print('#阿特巴希加密#')
                plank_text = input('请输入明码: ').upper()
                my_text = Atabash_cipher(plank_text)
                print(f'您的加密信息是:{my_text}')

            elif option == '3':
                print('#栅栏加密#')
                plank_text = input('请输入明码: ').upper()
                frequency = int(input('加密次数: '))
                my_text = Fail_fence_cipher(plank_text, frequency)
                print(my_text)
            elif option == '4':
                print('#维吉尼亚加密#')
                Vigenere_option = input('加密(E), 解密(D)').upper()
                if Vigenere_option == 'E':
                    plank_text = input('请输入明码: ').upper()
                else:
                    plank_text = input('请输入密码: ').upper()
                secret = input('请输入密钥: ').upper()
                my_text = Vigenere_cipher(plank_text, secret, Vigenere_option)
                if Vigenere_option == 'E':
                    print(f'您的加密信息是:{my_text}')
                else:
                    print(f'您的明码信息是:{my_text}')
            
            elif option == '5':
                print('#转换加密#')
                plank_text = input('请输入明码: ').upper()
                secret = input('请输入密钥') .upper()

                my_text = Transposition_cipher(plank_text, secret)
                print(f'您的加密信息是:{my_text}')
            else:
                ask = input('确定要退出使用?(y/n): ')
                if ask == 'y':
                    run = False
                    print('感谢您的使用')
        else:
            ask = input('确定要退出使用?(y/n): ')
            if ask == 'y':
                run = False
                print('感谢您的使用')


main()
